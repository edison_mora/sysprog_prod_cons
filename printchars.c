#include "csapp.h"
#include "sbuf.h"

void *funcionProductor(void *nombreArchivo);
void *funcionConsumidor(void *algo);

sbuf_t estructuraBuffer;
pthread_t productor,consumidor;
//sem_t mutexProductor, mutexConsumidor;
char *filename;
int contador = 0;


int main(int argc, char **argv)
{


	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];
	sbuf_init(&estructuraBuffer, 10);
	//Sem_init(&mutexProductor,0,1);
	//Sem_init(&mutexConsumidor,0,1);

	pthread_create(&productor, NULL, funcionProductor, NULL);
	pthread_create(&consumidor,NULL, funcionConsumidor, NULL);

	pthread_join(productor, NULL);
	pthread_join(consumidor,NULL);

	sbuf_deinit(&estructuraBuffer);



	
	return 0;
}


/* Create an empty, bounded, shared FIFO buffer with n slots */
void sbuf_init(sbuf_t *sp, int n)
{
	sp->buf = Calloc(n, sizeof(char));
	sp->n = n; /* Buffer holds max of n items */
	sp->front = sp->rear = 0; /* Empty buffer iff front == rear */
	Sem_init(&sp->mutex, 0, 1); /* Binary semaphore for locking */
	Sem_init(&sp->slots, 0, n); /* Initially, buf has n empty slots */
	Sem_init(&sp->items, 0, 0); /* Initially, buf has zero data items */
}

 /* Clean up buffer sp */
 void sbuf_deinit(sbuf_t *sp)
 {
 	Free(sp->buf);
 }

 /* Insert item onto the rear of shared buffer sp */
 void sbuf_insert(sbuf_t *sp, char item)
 {
	P(&sp->slots); /* Wait for available slot */
	P(&sp->mutex); /* Lock the buffer */
	sp->buf[(++sp->rear)%(sp->n)] = item; /* Insert the item */
	V(&sp->mutex); /* Unlock the buffer */
	V(&sp->items); /* Announce available item */
 }

 /* Remove and return the first item from buffer sp */
char sbuf_remove(sbuf_t *sp)
{
	char item;
	P(&sp->items); /* Wait for available item */
	P(&sp->mutex); /* Lock the buffer */
	item = sp->buf[(++sp->front)%(sp->n)]; /* Remove the item */
	V(&sp->mutex); /* Unlock the buffer */
	V(&sp->slots); /* Announce available slot */
	return item;
}

void *funcionProductor( void *nombreArchivo){
	int fd;
	char c;
	struct stat fileStat;


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		

		fd = Open(filename, O_RDONLY, 0);
		while(Read(fd,&c,1)){
			//P(&mutexProductor);
			contador++;
			usleep(50);
			sbuf_insert(&estructuraBuffer, c);
			//V(&mutexProductor);
		}			
		Close(fd);
	}




}

void *funcionConsumidor( void *algo){
	char letra;

	while(contador !=0){
		//P(&mutexConsumidor);
		sleep(1);
		letra = sbuf_remove(&estructuraBuffer);
		contador--;
		printf("Read: %c\n",letra);
			
		//V(&mutexConsumidor);


	}

}
